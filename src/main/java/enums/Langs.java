package enums;

public enum Langs {
    SWEDEN("se/en/home"),
    USA("us/en/home"),
    CHINA("cn/en/home");


    public String getUrlLang() {
        return urlLang;
    }

    //            {
//        public String toString() {
//            return "se/en/home";
//        }
//    },
//
//    USA {
//        public String toString() {
//            return "us/en/home";
//        }
//    },
//    CHINA {
//        public String toString() {
//            return "cn/en/home";
//        }
//    }
private String urlLang;

    Langs(String urlLang) {
        this.urlLang = urlLang;
    }

}
