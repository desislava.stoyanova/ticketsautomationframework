package enums;

public enum Stores {
    STOCKHOLM("se/en/stores/stockholm"),
    PARIS("/se/en/stores/paris"),
    CHINA("cn/en/home");

    public String getUrlStore() {
        return urlStore;
    }

    private String urlStore;

    Stores(String urlStore) {
        this.urlStore = urlStore;
    }
}
