package pages;

import enums.Checked;
import enums.Langs;
import org.asynchttpclient.util.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;

public class HomePage extends BasePage {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class);

    @FindBy(how = How.ID, using = "link__stores")
    private WebElement storesButton;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'nav-stores')]//span[contains(text(),\"Stockholm\")]")
    private WebElement stockholmStoreButton;
    @FindBy(how = How.ID, using = "register")
    private WebElement registerForm;

    @FindBy(how = How.XPATH, using = "//div[@class='modal-content']")
    private WebElement registerFormModal;

    @FindBy(how = How.XPATH, using = "//form[@id='register-form']//input[@placeholder='Name']")
    private WebElement usernameInputField;

    @FindBy(how = How.XPATH, using = "//form[@id='register-form']//input[@placeholder='Email']")
    private WebElement emailInputField;

    @FindBy(how = How.XPATH, using = "//form[@id='register-form']//input[@placeholder='Password']")
    private WebElement passwordInputField;

    @FindBy(how = How.XPATH, using = "//form[@id='register-form']//input[@placeholder='Verify password']")
    private WebElement verifyPasswordInputField;

    @FindBy(how = How.XPATH, using = "//form[@id='register-form']//label[1]")
    private WebElement termsOfUseCheckbox;

    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary' and contains(text(),'Register')]")
    private WebElement registerButton;

    @FindBy(how = How.XPATH, using = "//div[@class='slide welcome']")
    private WebElement welcomeModal;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnStoresButton(){

        LOGGER.info("Clicking Stores button");
        click(storesButton);
    }
    public void navigateToHomepage() {
        navigateTo();
    }

    public void verifyHomepageUrl() {
        verifyUrl("https://local.evedo.co/");
    }

    public void waitForRegisterFormToBeVisible(){
        waitForElementVisibility(registerForm);
    }

    public void clickOnTheRegisterForm(){
        click(registerForm);
    }

    public void waitForTheRegisterFormModalToBeVisible(){
        waitForElementVisibility(registerFormModal);
    }

    public void insertUsername(){
        typeText(usernameInputField, "Desislava");
    }
    public void insertEmail(){
        typeText(emailInputField, getRandomString(5) + "@test.com");
    }

    public void insertPassword(){
        typeText(passwordInputField, "test1234");
    }
    public void verifyPassword(){
        typeText(verifyPasswordInputField, "test1234");
    }

    public void checkTheTermsOfUseCheckbox(){
        checkCheckbox(termsOfUseCheckbox, Checked.YES); }

//        public void clickOnVerifyPasswordField(){
//        clickOnElement(verifyPasswordInputField);
//          }
    public void waitForTheVerifyPasswordFieldToBeInteractable(){
        waitForElementVisibility(verifyPasswordInputField);
    }

    public void waitForTheTermsCheckboxToBeInteractable(){
        waitForElementVisibility(termsOfUseCheckbox);
    }

    public void clickOnTheRegisterButton(){
        click(registerButton);
    }

    public void verifyProfilePageUrl(){
        verifyUrl("https://local.evedo.co/profile/setup");
    }

    public void waitForProfilePageToBeVisible(){
        waitForElementVisibility(welcomeModal);
    }

    public void hoverOnStoresButton(){
        hoverOnElement(storesButton);
    }

    public void navigateToSite(Langs lang){
        navigateTo(lang.getUrlLang());
    }
    public void clickOnStockholmStore(){
        click(stockholmStoreButton);
    }

    public void verifyHomePageUrl(String url){
        verifyUrl(url);
    }

    public void verifyHomePageTitle(String title){
        verifyTitle(title);
    }

    public void getProfilePageWelcomeModalText(){
        getText(welcomeModal);
    }

    int statusCode;
    public int getHomePageHTTPStatusCode(){
        getHttpResponseCode(BASE_URL);
        return  statusCode;
    }
    public void verifyHomePageStatusCode(){
        Assert.assertEquals(200, statusCode);
    }

    public void captureHomepageNetworkTraffic(){ captureNetworkTraffic(); }

    public void captureAnyAvailableErrors(){
        captureBrowserErrors();
    }

}
