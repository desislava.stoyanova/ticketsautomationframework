package ui;

import core.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


public class HomePageTests extends BaseTest {


    @Test
    @DisplayName("Successful registration")

    public void registerUser() {

        app.homePage().navigateToHomepage();
        app.homePage().verifyHomepageUrl();
        app.homePage().waitForRegisterFormToBeVisible();
        app.homePage().clickOnTheRegisterForm();
        app.homePage().waitForTheRegisterFormModalToBeVisible();
        app.homePage().insertUsername();
        app.homePage().insertEmail();
        app.homePage().insertPassword();
        app.homePage().waitForTheVerifyPasswordFieldToBeInteractable();
        app.homePage().verifyPassword();
        app.homePage().waitForTheTermsCheckboxToBeInteractable();
        app.homePage().checkTheTermsOfUseCheckbox();
        app.homePage().clickOnTheRegisterButton();
        app.homePage().waitForProfilePageToBeVisible();
        app.homePage().verifyProfilePageUrl();
//        app.homePage().verifyHomePageStatusCode();
        app.homePage().captureHomepageNetworkTraffic();
        app.homePage().captureAnyAvailableErrors();

    }
}
